import time
from os import listdir, mkdir, rename, path
from os.path import isfile, join
from re import compile
from enum import Enum
from datetime import datetime

folder = "D:\\QV\\QRam34\\"

regexKITFile = compile("^(Q[DV]Event|QD0\d\d)_H\d\dM\d\dS\d\d_D\d\d-M\d\d-Y\d\d\d\d(.Log|.RAW)$")
regexQDEvent = compile("^QDEvent_H\d\dM\d\dS\d\d_D\d\d-M\d\d-Y\d\d\d\d.Log$")
regexQVEvent = compile("^QVEvent_H\d\dM\d\dS\d\d_D\d\d-M\d\d-Y\d\d\d\d.Log$")
regexQData = compile("^QD0\d\d_H\d\dM\d\dS\d\d_D\d\d-M\d\d-Y\d\d\d\d(.Log|.RAW)$")

regexTimestamp = compile("H\d\dM\d\dS\d\d_D\d\d-M\d\d-Y\d\d\d\d")

uptime = 0
refresh = 2


class FileType(Enum):
    START = 0
    DATA = 1
    END = 2


print("QDataCollector ready to keep clean folder: " + folder)
while True:
    # find all interesting files in the folder
    files = [f for f in listdir(folder) if isfile(join(folder, f)) and
             regexKITFile.match(f)]

    kitFiles = []

    # save the list of files as tuples with type and timestamp properties
    for f in files:
        filetype = None
        if regexQDEvent.match(f):
            filetype = FileType.START
        elif regexQVEvent.match(f):
            filetype = FileType.END
        elif regexQData.match(f):
            filetype = FileType.DATA
        timestamp = regexTimestamp.findall(f)[0]
        date_time_obj = datetime.strptime(timestamp, "H%HM%MS%S_D%d-M%m-Y%Y")
        fileObject = (f, filetype, date_time_obj)
        kitFiles.append(fileObject)

    # sort files by date
    kitFiles.sort(key=lambda x: x[2])
    batches = []
    bStart = -1
    bEnd = 0
    index = 0

    # distinguish separate quench events
    for f in kitFiles:
        if f[1] == FileType.START:
            bStart = index
        elif f[1] == FileType.END:
            bEnd = index
            if bStart >= 0:
                batches.append([bStart, bEnd])
        index += 1
    if len(batches) > 0:
        time.sleep(refresh)
        uptime += refresh
        # move events to folder named as QDEvent file
        for b in batches:
            newFolder = join(folder, kitFiles[b[0]][2].strftime("Event_%d_%m_%Y__%H%M_%S"))
            index = 0
            suffix = ""
            while path.isdir(newFolder):
                index += 1
                suffix = "_copy"+str(index)
                newFolder = join(folder, kitFiles[b[0]][2].strftime("Event_%d_%m_%Y__%H%M_%S")+suffix)
            mkdir(newFolder)
            for i in range(b[0], b[1]+1):
                rename(join(folder, kitFiles[i][0]), join(newFolder, kitFiles[i][0]))
            print("\nMoved quench data to " + path.basename(newFolder))
    time.sleep(refresh)
    uptime += refresh
    print("\rUptime " + str(uptime) + " seconds", end='')
